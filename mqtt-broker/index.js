const AedesServer = require("aedes");
const redis = require("mqemitter-redis");
const ws = require("websocket-stream");

const aedesPersistenceRedis = require("aedes-persistence-redis");
const config = require("./configuration");

const mq = redis(config.persistence.redis);

const aedes = AedesServer({
  mq,
  persistence: aedesPersistenceRedis({
    ...config.persistence.redis,
    db: config.persistence.redis.db + 1,
  }),
});

const {
  authorizePublish,
  authorizeSubscribe,
  addTopic,
} = require("aedes-authorization-plugin");

// configure topic authorization with aedes
const vehicleDataTopic = "vehicles/+vehicleUuid/data";

const hasAccessToVehcileDataTopic = (client, sub) => {
  return true;
};

addTopic(vehicleDataTopic, (client) => client.isGateway, {
  isSubscriptionTopic: false,
  isPublishTopic: true,
});

addTopic(vehicleDataTopic, hasAccessToVehcileDataTopic, {
  isSubscriptionTopic: true,
  isPublishTopic: false,
});

// hook up auth plugin
aedes.authorizeSubscribe = authorizeSubscribe;
aedes.authorizePublish = authorizePublish;
aedes.authenticate = require("./authentication");

// setup event handlers
aedes.on("client", (client) => {
  console.log(`${client.id} connected`);
});

aedes.on("subscribe", (p, client) => {
  console.log(`${client.id} subscribed to ${p[0].topic}`);
});

aedes.on("unsubscribe", (p, client) => {
  console.log(`${client.id} unsubscribed from:`);
  console.log(p);
});

aedes.on("publish", (p, client) => {
  if (!p.topic.includes("$SYS")) {
    console.log(`${client.id} published to ${p.topic}`);
  }
});

// start up MQTT broker for MQTT and WS protocols

const mqttServer = require("net").createServer(aedes.handle);
mqttServer.listen(config.port, function () {
  console.log("MQTT broker listening on port", config.port);
});

const httpServer = require("http").createServer();
ws.createServer({ server: httpServer }, aedes.handle);
httpServer.listen(config.websocketPort, function () {
  console.log("WS broker listening on port", config.websocketPort);
});
