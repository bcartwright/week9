const config = require("../configuration");
const request = require("superagent");

module.exports = async (client, username, password, callback) => {
  console.log(username);

  password = password && password.toString();

  //creds not supplied
  if (!username || !password) {
    console.log("rejected");
    return callback(false, false);
  }

  //check if gateway is running
  if (
    username == config.gateway.credentials.username &&
    password.toString() == config.gateway.credentials.password
  ) {
    client.isGateway = true;
    return callback(null, true);
  }

  //check if its a user
  const authorizationHeader = `Bearer ${password.toString()}`;

  request
    .get(`${config.gateway.baseUrl}/users/${username}`)
    .set("Authorization", authorizationHeader)
    .send()
    .then((res) => {
      client.user = res.body;
      return callback(null, true);
    })
    .catch((err) => {
      console.log("reject");
      return callback(false, false);
    });
};
