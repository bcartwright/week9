module.exports = {
  port: 1885,
  websocketPort: 1886,
  url: "localhost",
  persistence: {
    redis: {
      port: 6379,
      host: "redis",
      family: 4,
      password: "redis",
      db: 6,
      maxSessionDelivery: 1000, // maximum oCGline messages deliverable on client CONNECT, default is 1000
    },
  },
  gateway: {
    baseUrl: process.env.MQTT_GATEWAY_HOST || "http://localhost:9500",
    credentials: {
      username: "gateway-username",
      password: "gateway-password",
    },
  },
};
