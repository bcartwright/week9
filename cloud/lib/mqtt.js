const mqtt = require("mqtt");

const config = require("../configuration");

module.exports.publishMqtt = ({ topic, message, retain = true }) => {
    return new Promise((resolve, reject) => {
        const client = mqtt.connect(
            `mqtt://${config.mqtt.host}:${config.mqtt.port}`,
            {
                username: config.mqtt.username,
                password: config.mqtt.password,
            }
        );

        client.on("connect", () => {
            client.publish(
                topic,
                JSON.stringify(message),
                { retain, qos: 1 },
                (err, granted) => {
                    if (err) {
                        reject(err);
                    }
                    client.end(true);
                    resolve(granted);
                }
            );
        });
    });
};

