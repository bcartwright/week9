//redis
const redis = require('redis');
const client = redis.createClient({ host: 'redis' });
const { promisify } = require('util')
module.exports.getCache = promisify(client.get).bind(client);
module.exports.setCache = promisify(client.set).bind(client);

//mongo
const MongoClient = require('mongodb').MongoClient;
const mongoUrl = 'mongodb://mongo:27017';
mongoDbName = 'FsseVehicleData'
let mongo;

MongoClient.connect(mongoUrl, { useUnifiedTopology: true }, function (
    err,
    client
) {
    mongo = client.db(mongoDbName);
});

module.exports.insertDocument = ({ collection, documents }) => {
    return new Promise((resolve, reject) => {
        let c = mongo.collection(collection);
        c.insertMany(documents, function (error, results) {
            if (error) {
                reject(error);
            }
            return resolve(results);
        })
    })
}

module.exports.findAll = ({ collection, documents, field, sorter }) => {
    return new Promise((resolve, reject) => {
        let c = mongo.collection(collection);
        c.find(documents, field).sort(sorter).toArray((function (error, results) {
            if (error) {
                return reject(error);
            }
            return resolve(results);
        }))
    })
}

module.exports.mongo = mongo;

//mysql
const mysql = require('mysql');

const pool = mysql.createPool({
    host: 'mysql',
    user: "root",
    password: 'root',
    database: 'FSSE_Vehicle',
    connectionLimit: 10
})

module.exports.query = function query({ sql, values }) {
    return new Promise((resolve, reject) => {
        pool.query(sql, values, function (error, results, field) {
            if (error) {
                return reject(error)
            }
            return resolve(results)
        })
    }).catch((err) => { console.log({ err }) })
}