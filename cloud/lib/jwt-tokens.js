const jwt = require('jwt-simple');
const moment = require('moment');

const config = require('../configuration');

const createUserToken = ({ userInfo }) => {
    const exp = moment().add(config.jwt.user.expiration, 'second').valueOf();

    const token = jwt.encode(
        {
            sub: JSON.stringify(userInfo),
            iat: Date.now(),
            exp
        },
        config.jwt.user.secret
    )
    return { token, tokenExpires: exp }
}

const verifyAndDecodeUserToken = ({ token }) => {
    const decoded = jwt.decode(token, config.jwt.user.secret);
    const userInfo = JSON.parse(decoded.sub);
    return userInfo;
}

module.exports = { createUserToken, verifyAndDecodeUserToken }




// function _getJwtTokenParts({ token }) {
//     const tokenParts = token.split('.');
//     return {
//         head: tokenParts[0],
//         payload: tokenParts[1],
//         sig: tokenParts[2]
//     }
// }

// let userInfo = {
//     uuid: 'blah@360d7f6c-5742-4cb4-8c4c-d258bf12bbcf.com',
//     email: 'rando@email.com'
// }

// let { token } = createUserToken({ userInfo });

// let decodedUserToken = verifyAndDecodeUserToken({ token });
// console.log(decodedUserToken);

// let tokenParts = _getJwtTokenParts({ token });
// console.log(tokenParts);

// let decodeHeader = Buffer.from(tokenParts.head, 'base64').toString('utf-8');
// console.log(decodeHeader);

// let decodePayload = Buffer.from(tokenParts.payload, 'base64').toString('utf-8');
// console.log(decodePayload);

// let sigHex = Buffer.from(tokenParts.sig, 'base64').toString('hex');
// console.log({
//     hex: sigHex,
//     hexLength: sigHex.length,
//     nBits: sigHex.length * 4
// })