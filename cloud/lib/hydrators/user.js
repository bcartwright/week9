const _ = require('lodash');
const { query } = require('../database');

module.exports.hydrateUser = async (record) => {

    record.uuid = record.uuid.toString();

    //remove id & password
    delete record.id
    delete record.password

    return record;
}
