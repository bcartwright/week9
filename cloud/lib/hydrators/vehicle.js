const _ = require('lodash');
const { query } = require('../database');
const { hydrateVehicleModel } = require('./vehicleModel');

module.exports.hydrateVehicle = async (vehicle) => {
    vehicle.uuid = vehicle.uuid.toString();
    vehicle.rbg = JSON.parse(vehicle.rbg);

    //get model uuid
    const { modelId } = vehicle;
    const modelRecordList = await query({
        sql: `SELECT * FROM VehicleModel WHERE id=?`,
        values: [modelId]
    })
    const modelRecord = _.head(modelRecordList);
    const model = await hydrateVehicleModel(modelRecord);
    vehicle.modelUuid = modelRecord.uuid.toString();
    vehicle.model = model;

    // //add make & modelName from VehicleModel DB
    // const make = modelRecord.make
    // const modelName = modelRecord.modelName
    // vehicle['make'] = make;
    // vehicle['modelName'] = modelName;

    //delete id
    delete vehicle.id;
    delete vehicle.modelId;

    return vehicle;
}

