const _ = require('lodash');
const { query } = require('../database');

module.exports.hydrateVehicleModel = async (record) => {
    record.uuid = record.uuid.toString();

    //remove ids
    delete record.id;

    return record;
}