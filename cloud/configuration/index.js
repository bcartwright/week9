module.exports = {
  port: process.env.PORT || 9500,
  jwt: {
    user: {
      secret: process.env.USER_TOKEN_SECRET || "supersecret",
      expiration: process.env.USER_TOKEN_EXPIRATION || 3600 * 24 * 30, // one month
    },
  },
  mqtt: {
    host: process.env.MQTT_HOST || "192.168.1.19",
    port: process.env.MQTT_PORT || 1885,
    username: process.env.MQTT_USERNAME || "gateway-username",
    password: process.env.MQTT_PASSWORD || "gateway-password",
  },
};
