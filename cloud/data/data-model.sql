DROP DATABASE IF EXISTS `FSSE_Vehicle`;
CREATE DATABASE IF NOT EXISTS `FSSE_Vehicle`
    DEFAULT CHARACTER SET utf8
    COLLATE utf8_general_ci;
USE `FSSE_Vehicle`;

DROP TABLE IF EXISTS `User`;
CREATE TABLE IF NOT EXISTS `User` (
  `id`                BIGINT        NOT NULL AUTO_INCREMENT,
  `uuid`              BINARY(36)    NOT NULL,
  `firstName`      	  VARCHAR(255)  NOT NULL,
  `lastName`      	  VARCHAR(255)  NOT NULL,
  `email`             VARCHAR(255)  NOT NULL,
  `password`          VARCHAR(1024) NOT NULL,
  `img`               TEXT NULL,
  `created`           TIMESTAMP    DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY id_pk (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


INSERT INTO User SET uuid='e7f39e47-e076-4369-a248-2691584cf285', firstName='Pippin', lastName='Cartwright', email='pippindoge@email.com', password=SHA2('pipdoggydog', 256);

DROP TABLE IF EXISTS `VehicleModel`;
CREATE TABLE IF NOT EXISTS `VehicleModel` (
  `id`   BIGINT NOT NULL AUTO_INCREMENT,
  `uuid` binary(36) NOT NULL UNIQUE,
  `name` VARCHAR(20) NULL,
  `number` VARCHAR(50) NULL,
  `make` VARCHAR(50) NULL,
  `powertrainType` ENUM('V4', 'V6', 'BEV', 'HEV') NOT NULL DEFAULT 'BEV',
  `startYear` INTEGER NULL,
  `endYear` INTEGER NULL,
  `description` TEXT NULL,
  `imageUrl` TEXT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY id_pk (`id`)
) ENGINE=MYISAM CHARSET=utf8;

INSERT INTO VehicleModel SET uuid="0c22491b-79b0-4214-a524-50311cd54fdb", name="Focus", make="Ford",      startYear=1972, endYear=2020;
INSERT INTO VehicleModel SET uuid="0fefb9e9-4798-4545-a88e-2875c9eb092e", name="Carolla", make="Toyota",     startYear=1998, endYear=2020;
INSERT INTO VehicleModel SET uuid="5c78c827-4559-48fe-921b-5c98ee7077c6", name="Bolt",  make="Chevrolet", startYear=2016, endYear=2020;


DROP TABLE IF EXISTS `Vehicle`;
CREATE TABLE IF NOT EXISTS `Vehicle` (
  `id`   BIGINT NOT NULL AUTO_INCREMENT,
  `uuid` binary(36) NOT NULL UNIQUE,
  `name` VARCHAR(20) NULL,
  `modelId` BIGINT NULL,
  `vin`  VARCHAR(50) NOT NULL,
  `year` INTEGER NULL,
  `description` TEXT NULL,
  `color` TEXT NULL,
  `rbg`  JSON NULL,
  `imageUrl` TEXT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `Vehicle_modelId_fk`
    FOREIGN KEY (`modelId`)
    REFERENCES `VehicleModel` (`id`),

  PRIMARY KEY id_pk (`id`)
) ENGINE=MYISAM CHARSET=utf8;

DROP TABLE IF EXISTS `UserVehicle`;
CREATE TABLE IF NOT EXISTS `UserVehicle` (
  `id`        BIGINT NOT NULL AUTO_INCREMENT,
  `uuid`      binary(36) NOT NULL UNIQUE,
  `userId`    BIGINT NOT NULL,
  `vehicleId` BIGINT NOT NULL,
  `created`   timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated`   timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  CONSTRAINT `userVehicle_userId_fk`
    FOREIGN KEY (`userId`)
    REFERENCES `User` (`id`),
  CONSTRAINT `userVehicle_vehicleId_fk`
    FOREIGN KEY (`vehicleId`)
    REFERENCES `Vehicle` (`id`),
  PRIMARY KEY id_pk (`id`)
)
ENGINE=MYISAM CHARSET=utf8;