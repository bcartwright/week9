const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const uuid = require("uuid");
const cors = require("cors");
const redis = require("redis");
const _ = require("lodash");
const SHA2 = require("sha2");
const db = require("mongodb");
const { encode, decode } = require("base64-arraybuffer");
const ab2str = require("arraybuffer-to-string");

//hydraters & tools
const { hydrateVehicle } = require("./lib/hydrators/vehicle");
const { hydrateUser } = require("./lib/hydrators/user");
const {
  query,
  insertDocument,
  findAll,
  getCache,
  setCache,
} = require("./lib/database");
const { createUserToken } = require("./lib/jwt-tokens");
const { publishMqtt } = require("./lib/mqtt");

//middleware
const { vehicleDataPostValidator } = require("./middleware/validators");
const { newUserDataPostValidator } = require("./middleware/validators");

const { openApiVehicleDataPostSchema } = require("./middleware/schemas");
const { openApiNewUserDataPostSchema } = require("./middleware/newUserSchema");
const { checkAndAttachUser } = require("./middleware/checkTokens");
const { isSameUser } = require("./middleware/isSameUser");

const openapi = require("@wesleytodd/openapi");
const { find, isInteger } = require("lodash");

const PORT = 9500;

const app = express();

app.use(morgan("tiny"));
app.use(bodyParser.json());
app.use(cors());

const oapi = openapi({
  openapi: "3.0.0",
  info: {
    title: "Vehicle API",
    description: "API for AV",
    version: "1.0.0",
  },
});

app.use(oapi);

//NO AUTH & Utils
app.post("/login", async (req, res) => {
  const { email, password } = req.body;

  const userRecordList = await query({
    sql: `SELECT * FROM User WHERE email=? AND password=SHA2(?, 256)`,
    values: [email, password],
  });

  if (!userRecordList.length) {
    return res.status(401).json({ status: "Unauthorized" });
  }

  const userRecord = _.head(userRecordList);

  //hydration
  const user = await hydrateUser(userRecord);

  //create token
  const { token, tokenExpiration } = createUserToken({
    userInfo: { uuid: user.uuid, email: user.email },
  });

  const out = { user, token, tokenExpiration };

  return res.json(out);
});
app.post(
  "/newUser/:email",
  oapi.path(openApiNewUserDataPostSchema),
  newUserDataPostValidator,
  async (req, res) => {
    const newUserUuid = uuid.v4();
    req.body.uuid = newUserUuid;
    const hexedPassword = SHA2["SHA-256"](req.body.password);
    req.body.password = hexedPassword.toString("hex");
    await query({
      sql: `INSERT INTO User SET ?`,
      values: [req.body],
    }).catch((err) => {
      console.log({ status: "Failed to add user" });
      console.log(err);
    });
    res.status(201).json("user added");
  }
);
app.get(
  "/users/:userUuid",
  checkAndAttachUser,
  isSameUser,
  async (req, res) => {
    return res.json(req.user);
  }
);
app.get("/health-check", (req, res) => {
  res.json({ status: "ok!" });
});
app.listen(PORT, () => {
  console.log(`cloud listening on ${PORT}`);
});
app.use("/docs", oapi.redoc);
app.use("/swagger", oapi.swaggerui);

//AUTH ROUTES
app.post("/provision/:vin", checkAndAttachUser, async (req, res) => {
  const vehicleUuid = uuid.v4();
  req.body.uuid = vehicleUuid;
  req.body.rbg = JSON.stringify(req.body.rbg);

  const { modelUuid } = req.body;
  delete req.body.modelUuid;

  await query({
    sql: `INSERT INTO Vehicle SET ? , modelId=(SELECT id FROM VehicleModel WHERE uuid=?)`,
    values: [req.body, modelUuid],
  });

  //get created vehicle
  const recordList = await query({
    sql: `SELECT * FROM Vehicle WHERE uuid=?`,
    values: [vehicleUuid],
  });
  const record = _.head(recordList);

  //Set user to vehicle
  const userVehicle = {
    uuid: uuid.v4(),
    userId: req.userRecord.id,
    vehicleId: record.id,
  };
  await query({
    sql: `INSERT INTO UserVehicle SET ?`,
    values: [userVehicle],
  });

  //hydrate vehicle record
  const out = await hydrateVehicle(record);

  res.status(201).json(out);
});

app.get("/vehicles", checkAndAttachUser, async (req, res) => {
  let results = await query({ sql: "SELECT * FROM Vehicle", values: [] });

  let hydratedResults = await Promise.all(
    results.map((r) => {
      return hydrateVehicle(r);
    })
  );
  console.log(hydratedResults);
  res.json(hydratedResults);
});

app.get("/vehicles/:vehicleUuid", checkAndAttachUser, async (req, res) => {
  let results = await query({
    sql: "SELECT * FROM Vehicle WHERE uuid=?",
    values: [req.params.vehicleUuid],
  });
  res.json(results);
});

app.post(
  "/vehicles/:vehicleUuid/data",
  checkAndAttachUser,
  oapi.path(openApiVehicleDataPostSchema),
  vehicleDataPostValidator,
  async (req, res) => {
    req.body.vehicleUuid = req.params.vehicleUuid;
    insertDocument({ collection: "VehicleData", documents: [req.body] });

    setCache(req.params.vehicleUuid, JSON.stringify(req.body));

    await publishMqtt({
      topic: `vehicles/${req.params.vehicleUuid}/data`,
      message: req.body,
      retain: true,
    });

    return res.json();
  }
);

app.get(
  "/vehicles/:vehicleUuid/latest-data",
  checkAndAttachUser,
  async (req, res) => {
    const latestData = await getCache(req.params.vehicleUuid);
    if (!latestData) return res.json({ status: "No data available" });
    res.json(JSON.parse(latestData));
  }
);

app.get(
  "/vehicles/:vehicleUuid/top-speed",
  checkAndAttachUser,
  async (req, res) => {
    const speed = await Promise.all([
      findAll({
        collection: "VehicleData",
        documents: { vehicleUuid: { $all: [req.params.vehicleUuid] } },
        field: { fields: { _id: 0, vehicleSpeed: 1 } },
        sorter: { vehicleSpeed: 1 },
      }),
    ]);
    let speed2 = speed[0];
    const speed3 = speed2.pop();
    if (!speed3) return res.json({ status: "No speed data available" });
    res.json(speed3);
  }
);

//Unused
app.patch("/users/:userUuid/imgData", async (req, res) => {
  await query({
    sql: `UPDATE User SET img=? WHERE uuid=?`,
    values: [req.body.imgData, req.params.userUuid],
  }).catch((err) => {
    console.log("Failed to add img to db"), console.log(err);
  });
  res.status(201).json("Img load successful");
});

app.get("/users/:userUuid/imgString", async (req, res) => {
  let user = await query({
    sql: `SELECT img FROM User WHERE uuid=?`,
    values: [req.params.userUuid],
  });
  const userImageBase64 = user[0].img;
  // console.log(userImageBase64);
  // let bufferObj = Buffer.from(userImageBase64, "base64");
  // let decodedString = bufferObj.toString("ascii");
  res.json(userImageBase64);
});
