const Ajv = require('ajv').default;

const { vehicleDataPostSchema } = require('./schemas');
const { newUserDataPostSchema } = require('./newUserSchema');


module.exports.vehicleDataPostValidator = (req, res, next) => {
    // var valid = ajv.validate(vehicleDataPostSchema, req.body)
    // if (!valid) {
    //     return res.status(400).json(ajv.errors);
    // } else {
    //     next();
    // }
    const ajv = new Ajv();
    const validate = ajv.compile(vehicleDataPostSchema)
    const valid = validate(req.body)
    if (!valid) {
        console.log(validate.errors)
        return res.status(400).json(validate.errors);
    } else {
        next();
    }
}
//it seems this is the correct format
module.exports.newUserDataPostValidator = (req, res, next) => {
    const ajv = new Ajv();
    const validate = ajv.compile(newUserDataPostSchema)
    const valid = validate(req.body)
    if (!valid) {
        console.log(validate.errors)
        return res.status(400).json(validate.errors);
    } else {
        next();
    }
}