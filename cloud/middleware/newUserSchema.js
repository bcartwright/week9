module.exports.newUserDataPostSchema = {
    description: 'New user data post object',
    type: 'object',
    properties: {
        firstname: { type: 'string' },
        lastname: { type: 'string' },
        email: { type: 'string' },
        password: { type: 'string' },
    },
    required: ['firstname', 'lastname', 'email', 'password'],
    additionalProperties: false
}

module.exports.openApiNewUserDataPostSchema = {
    tags: ['New user data'],
    summary: 'Post new user data',
    requestBody: {
        description: 'Posts new user data',
        content: {
            'application/json': {
                schema: {
                    description: 'New user data post object',
                    type: 'object',
                    properties: {
                        firstname: { type: 'string' },
                        lastname: { type: 'string' },
                        email: { type: 'string' },
                        password: { type: 'string' },
                    },
                    required: ['firstname', 'lastname', 'email', 'password'],
                    additionalProperties: false
                }
            }
        }
    }
}