const { verifyAndDecodeUserToken } = require("../lib/jwt-tokens");
const { query } = require("../lib/database");
const _ = require("lodash");
const { hydrateUser } = require("../lib/hydrators/user");

module.exports.checkAndAttachUser = async (req, res, next) => {
  const { authorization } = req.headers;

  let token = null;

  if (authorization) {
    token = authorization.replace("Bearer ", "");
  }

  if (!token) {
    return res.status(401).json({ status: "Unauthorized" });
  }

  //verify the token
  let decodedTokenData;
  try {
    decodedTokenData = verifyAndDecodeUserToken({ token });
  } catch (err) {
    console.log(err);
    return res.status(401).json({ status: "Not a valid token" });
  }

  //attach user to req & hydrate (for id)
  let userUuid = decodedTokenData.uuid;
  const userRecordList = await query({
    sql: `SELECT * FROM User WHERE uuid=?`,
    values: [userUuid],
  });

  const userRecord = _.head(userRecordList);
  req.userRecord = { ...userRecord };

  const user = await hydrateUser(userRecord);
  req.user = { ...user };

  return next();
};
