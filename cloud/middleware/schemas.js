module.exports.vehicleDataPostSchema = {
  description: "Vehicle data post object",
  type: "object",
  properties: {
    timestamp: { type: "integer" },
    vehicleSpeed: { type: "number" },
    acceleration: { type: "number" },
    distance: { type: "number" },
    positionX: { type: "number" },
    positionY: { type: "number" },
  },
  required: ["timestamp", "vehicleSpeed"],
  additionalProperties: false,
};

module.exports.openApiVehicleDataPostSchema = {
  tags: ["Vehicle Data"],
  summary: "Post vehicle body",
  requestBody: {
    description: "Posts data to vehicle contract",
    content: {
      "application/json": {
        schema: {
          description: "Vehicle data post object",
          type: "object",
          properties: {
            timestamp: { type: "integer" },
            vehicleSpeed: { type: "number" },
            acceleration: { type: "number" },
            distance: { type: "number" },
            positionX: { type: "number" },
            positionY: { type: "number" },
          },
          required: ["timestamp", "vehicleSpeed"],
          additionalProperties: false,
        },
      },
    },
  },
};
