module.exports.isSameUser = async (req, res, next) => {
    const userUuidParams = req.params.uuid;
    const userUuidAttached = req.body.uuid;

    if (userUuidAttached != userUuidParams) {
        return res.status(401).json({ status: 'Unauthorized' })
    }

    return next();
}