import requests

user_creds = {
    "email": "pippindoge@email.com",
    "password": "pipdoggydog"
}

new_user_list = [{
    # "firstname": "Joe",
    # "lastname": "Smith",
    # "email": "joey@email.com",
    # "password": "thenamesjoe",
}]

vehicle_model_uuid_dict = {
"focus": "0c22491b-79b0-4214-a524-50311cd54fdb",
"carolla": "0fefb9e9-4798-4545-a88e-2875c9eb092e",
"bolt": "5c78c827-4559-48fe-921b-5c98ee7077c6"
}

vehicle_list =  [
    {
        'vin': '12IJ92048k144003',
        'name': 'Dearborn',
        'modelUuid': vehicle_model_uuid_dict['focus'],
        'year': 2020,
        'color': 'blue',
        'rbg': {'r': 0, 'b': 255, 'g': 0},
        'imageUrl': 'https://www.motoringresearch.com/wp-content/uploads/2020/06/Ford_Focus_2020_02-1500x844.jpg'
    },
    {
        'vin': '24DOI490589D321A',
        'name': 'Tokyo',
        'modelUuid': vehicle_model_uuid_dict['carolla'],
        'year': 2020,
        'color': 'green',
        'rbg': {'r': 0, 'b': 0, 'g': 255},
        'imageUrl': 'https://cimg2.ibsrv.net/ibimg/hgm/1920x1080-1/100/645/toyota-corolla-im_100645394.jpg'
    },
    {
        'vin': '14EINOQ7VKJ900E9',
        'name': 'Detroit',
        'modelUuid': vehicle_model_uuid_dict['bolt'],
        'year': 2020,
        'color': 'Red',
        'rbg': {'r': 255, 'b': 0, 'g': 0},
        'imageUrl': 'https://electrek.co/wp-content/uploads/sites/3/2020/03/2020-Chevrolet-BoltEV-002-2000.jpg?quality=82&strip=all'
    }
]

# run health check
r = requests.get("http://192.168.1.19:9500/health-check")
health_check = r.json()
print("health check result:")
print(health_check["status"])

# #create new user
# for user in new_user_list:
#     new_email = user["email"]
#     print(f'Adding user for {new_email}')
#     m = requests.post(f"http://localhost:9500/newUser/{new_email}", json=user)

# login user
r = requests.post("http://192.168.1.19:9500/login", json=user_creds)
login_data = r.json()
token = login_data["token"]
print("login successful!")
print(token)

req_headers = {"Authorization": f"Bearer {token}"}
print(req_headers)

for vehicle in vehicle_list:
    vin = vehicle["vin"]
    print(f"Provisioning vehicle {vin}")
    p = requests.post(f"http://192.168.1.19:9500/provision/{vin}", headers=req_headers, json=vehicle)
    print(p.json())