from time import time, sleep
import requests
import sys
import os

tools = os.path.join(os.environ['SUMO_HOME'], 'share/sumo/tools')
sys.path.append(tools)

import traci

sumoBinary = "sumo-gui"
sumoCmd = [sumoBinary, "-c", "first_network.sumocfg",
           "--start", "--time-to-teleport", "100000000"]

api_base_url = "https://api.brycart.com"


# # Vehicle comes of manufacturing line
# VIN = 'sumo_car'
# veh = requests.post ('http://localhost:9500/provision/'+VIN)

# # Save assigned ID (uuid)
# vehicle = veh.json()
# print(vehicle)
# vehicle_id = vehicle['vehicleId']
# export SUMO_HOME=/usr/local/Cellar/sumo/1.8.0/

user_creds = {
    'email': 'pippindoge@email.com',
    'password': 'pipdoggydog'
}
# login user
r = requests.post(f"{api_base_url}/login", json=user_creds)
login_data = r.json()
token = login_data["token"]
req_headers = {"Authorization": f"Bearer  {token}"}

traci.start(sumoCmd)

#get vehicle list from backend
res = requests.get(f"{api_base_url}/vehicles", headers=req_headers)
vehicles_list = res.json()

for vehicle in vehicles_list:
    print('Got vehicle {0} information from cloud'.format(vehicle['name']))
    traci.vehicle.add(vehicle['uuid'], 'route1')
    color = (vehicle['rbg']['r'], vehicle['rbg']['b'], vehicle['rbg']['g'])
    traci.vehicle.setColor(vehicle['uuid'], color)
    print('initial speed:', traci.vehicle.getSpeed(vehicle['uuid']))


step = 1
while step < 10000:
    traci.simulationStep()

    for vehicle in vehicles_list:
        vehicle_id = vehicle['uuid']
        try:
            speed = traci.vehicle.getSpeed(vehicle_id)
            accel = traci.vehicle.getAcceleration(vehicle_id)
            distance = traci.vehicle.getDistance(vehicle_id)
            posX, posY = traci.vehicle.getPosition(vehicle_id)
        except:
            print('Vehicle has left the simulation')
            break

        print('Vehicle {0} speed: {1}'.format(vehicle_id, speed))

        out = {
            'timestamp': int(time()),
            'vehicleSpeed': speed,
            'acceleration': accel,
            'distance': distance,
            'positionX': posX,
            'positionY': posY
         }

        requests.post(f"{api_base_url}/vehicles/{vehicle_id}/data", headers=req_headers, json=out)

        current_road = traci.vehicle.getRoadID(vehicle_id)
        if current_road == 'top_east':
            traci.vehicle.setStop(vehicle_id, 'east_south', duration=5)

    step += 1
    sleep(0.5)

print('done!')
