appdirs==1.4.4
certifi==2020.11.8
chardet==3.0.4
dbus-python==1.2.16
distlib==0.3.1
distro==1.4.0
distro-info===0.23ubuntu1
filelock==3.0.12
idna==2.10
PyGObject==3.36.0
python-apt==2.0.0+ubuntu0.20.4.1
requests==2.25.0
requests-unixsocket==0.2.0
six==1.14.0
ssh-import-id==5.10
unattended-upgrades==0.1
urllib3==1.26.2
virtualenv==20.2.1
