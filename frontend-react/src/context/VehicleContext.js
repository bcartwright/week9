import React, { createContext, useEffect, useState } from "react";

export const VehicleContext = createContext();

const VehicleContextProvider = (props) => {
  const [vehicleList, setVehicleList] = useState(() => {
    let localData = localStorage.getItem("vehicleContext");
    localData = localData ? JSON.parse(localData) : { vehicleList: [] };
    return localData.vehicleList;
  });

  const [vehicleData, setVehicleData] = useState(() => {
    let localData = localStorage.getItem("vehicleContext");
    localData = localData ? JSON.parse(localData) : { vehicleData: {} };
    return localData.vehicleData;
  });

  useEffect(() => {
    const vehicleContext = { vehicleList, vehicleData };
    localStorage.setItem("vehicleContext", JSON.stringify(vehicleContext));
  }, [vehicleList, vehicleData]);

  return (
    <VehicleContext.Provider
      value={{ vehicleList, setVehicleList, vehicleData, setVehicleData }}
    >
      {props.children}
    </VehicleContext.Provider>
  );
};

export default VehicleContextProvider;
