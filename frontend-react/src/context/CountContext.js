import React, { createContext, useEffect, useState, useReducer } from "react";

const initialState = { count: 0 };
export const CountContext = createContext(initialState);

let reducer = (state, action) => {
  switch (action.type) {
    case "increment":
      return { ...state, count: state.count + 1 };
    case "decrement":
      return { ...state, count: state.count - 1 };
    case "reset":
      return { ...state, count: state.count == 0 };
    default:
      return;
  }
};

const CountContextProvider = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <CountContext.Provider value={{ state, dispatch }}>
      {props.children}
    </CountContext.Provider>
  );
};

export default CountContextProvider;
