const baseUrl =
  process.env.REACT_APP_API_BASE_URL || "http://192.168.1.19:9500";
export const wsBaseUrl =
  process.env.REACT_APP_MQTT_BASE_URL || "ws://192.168.1.19:1886";

//send HTTP get req to API
export const get = ({ route, headers = {} }) => {
  const url = `${baseUrl}${route}`;

  return fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      ...headers,
    },
  })
    .then((res) => {
      return res.json();
    })
    .catch((err) => {
      return console.log("unable to fetch data from api");
    });
};

export const post = ({ route, headers, body }) => {
  const url = `${baseUrl}${route}`;

  return fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      ...headers,
    },
    body: JSON.stringify(body),
  })
    .then((res) => {
      if (res.status >= 400) throw res;
      return res.json();
    })
    .catch((err) => {
      return console.log("unable to fetch data from api:", err);
    });
};

export const patch = ({ route, body }) => {
  const url = `${baseUrl}${route}`;

  return fetch(url, {
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  })
    .then((res) => {
      if (res.status >= 400) throw res;
      return res.json();
    })
    .catch((err) => {
      return console.log("unable to fetch data from api:", err);
    });
};
