import React from "react";
import { Link } from "react-router-dom";
import styles from "./Navbar.module.css";

class Navbar extends React.Component {
  render() {
    return (
      <div className={styles.navContainer}>
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/vehicles">Vehicles</Link>
            </li>
            <li>
              <Link to="/account">My Account</Link>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

export default Navbar;
