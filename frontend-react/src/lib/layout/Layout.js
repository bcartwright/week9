import React from "react";
import Navbar from "../navbar/Navbar";
import styles from "../layout/Layout.module.css";
import imgLogo from "../../assets/carLogo.png";

const Layout = (props) => {
  return (
    <div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <img src={imgLogo} alt="logo" style={{ maxHeight: 50 }} />
        <h1 style={{ marginLeft: 10 }}>Fullstack Systems Engineering Viewer</h1>
      </div>
      <Navbar />
      <main>{props.children}</main>
    </div>
  );
};
export default Layout;
