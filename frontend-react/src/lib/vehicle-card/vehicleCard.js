import React, { useState, useEffect, useContext } from "react";
import styles from "./vehicles.module.css";
import moment from "moment";

import { CountContext } from "../../context/CountContext";

const VehicleCard = (props) => {
  const { state, dispatch } = useContext(CountContext);
  const { vehicle, vehicleData, vehicleTopSpeed } = props;
  const vehicleHeading = `${vehicle.year} - ${vehicle.model.make} ${vehicle.model.name}`;

  return (
    <div key={vehicle.uuid} className={styles.card}>
      <div
        className={styles.cardImg}
        style={{ backgroundImage: `url(${vehicle.imageUrl})` }}
      ></div>
      <h3>{vehicleHeading}</h3>
      <div>
        <div>
          <span className={styles.label}>Name</span>
          {": "}
          <span className={styles.value}>{vehicle.name}</span>
        </div>
        <div>
          <span className={styles.label}>Color</span>
          {": "}
          <span className={styles.value}>{vehicle.color}</span>
        </div>
        <div>
          <span className={styles.label}>Last Report</span>
          {": "}
          <span className={styles.value}>
            {moment(vehicleData[vehicle.uuid]?.timestamp * 1000).fromNow()}
          </span>
        </div>
        <div>
          <span className={styles.label}>Top Speed</span>
          {": "}
          <span className={styles.value}>
            {vehicleTopSpeed[vehicle.uuid]?.vehicleSpeed?.toFixed(1)}m/s
          </span>
          <button
            onClick={() => dispatch({ type: "increment" })}
            style={{ marginLeft: 15 }}
          >
            Get Latest
          </button>
          <button
            onClick={() => dispatch({ type: "reset" })}
            style={{ marginLeft: 15 }}
          >
            Reset
          </button>
        </div>
        <div>
          <span className={styles.label}>Latest Speed</span>
          {": "}
          <span className={styles.value}>
            {vehicleData[vehicle.uuid]?.vehicleSpeed?.toFixed(1)}m/s
          </span>
        </div>
      </div>
      <div>
        <span className={styles.uuid}>Uuid</span>
        {": "}
        <span className={styles.uuid}>{vehicle.uuid}</span>
      </div>
    </div>
  );
};

export default VehicleCard;
