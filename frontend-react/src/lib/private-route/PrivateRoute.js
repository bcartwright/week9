import React, { Component, useContext, useEffect } from "react";

import { Route, Redirect } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";
import { VehicleContext } from "../../context/VehicleContext";
import VehicleContextProvider from "../../context/VehicleContext";

import * as api from "../../utils/api";

//higer order component
const withVehicleData = (Component) => {
  return (props) => {
    const { isAuthenticated, token } = useContext(AuthContext);
    const { setVehicleData, setVehicleList } = useContext(VehicleContext);

    useEffect(() => {
      async function onComponentMount() {
        let jsonData = await api.get({
          route: "/vehicles",
          headers: { Authorization: `Bearer ${token}` },
        });
        let vData = {};

        for (let i = 0; i < jsonData.length; i++) {
          let latestData = await api.get({
            route: `/vehicles/${jsonData[i].uuid}/latest-data`,
            headers: { Authorization: `Bearer ${token}` },
          });
          vData[jsonData[i].uuid] = latestData;
        }
        setVehicleData(vData);
        setVehicleList(jsonData);
      }
      if (isAuthenticated) {
        onComponentMount();
      }
    }, [isAuthenticated]);
    return <Component {...props} />;
  };
};

const PrivateRoute = (props) => {
  const { children, ...otherProps } = props;
  const { isAuthenticated } = useContext(AuthContext);

  const EnhancedRoute = withVehicleData(Route);

  return isAuthenticated ? (
    <VehicleContextProvider>
      <EnhancedRoute {...otherProps}>{children}</EnhancedRoute>
    </VehicleContextProvider>
  ) : (
    <Redirect
      to={{
        pathname: "/login",
        search: new URLSearchParams({
          to: props.location.search
            ? props.location.pathname + props.location.search
            : props.location.pathname,
        }).toString(),
      }}
    />
  );
};
export default PrivateRoute;
