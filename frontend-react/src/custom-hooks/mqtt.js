import React from 'react';
import mqtt from 'mqtt';

//mqtt hook
export const useMqtt = function (
    { url, opts,
        onmessage = (topic, payload) => { },
        onerror = () => { },
        onconnect = (client) => { }
    },
    dependencies
) {
    let Client = React.useRef();

    React.useEffect(() => {
        Client.current = mqtt.connect(url, opts);
        Client.current.on('message', (topic, payload) => {
            return onmessage(topic, payload.toString());
        });
        Client.current.on('error', onerror);
        Client.current.on('connect', () => {
            return onconnect(Client.current);
        });
    }, [...dependencies]);
};
