import React from "react";
//import './App.css';
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Home from "./screens/home/home";
import Vehicles from "./screens/vehicles/vehicles";
import Login from "./screens/login/Login";
import Account from "./screens/account/account";

import Layout from "./lib/layout/Layout";
import PrivateRoute from "./lib/private-route/PrivateRoute";

import AuthContextProvider from "./context/AuthContext";
import UserContextProvider from "./context/UserContext";
import CountContextProvider from "./context/CountContext";
import createNewUser from "./screens/createAccount/createAccount";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AuthContextProvider>
          <UserContextProvider>
            <CountContextProvider>
              <Switch>
                <Route path="/login" exact component={Login} />
                <Route path="/newUser" exact component={createNewUser} />
                <PrivateRoute path="/" exact>
                  <Layout>
                    <Home />
                  </Layout>
                </PrivateRoute>
                <PrivateRoute path="/home" exact>
                  <Layout>
                    <Home />
                  </Layout>
                </PrivateRoute>
                <PrivateRoute path="/vehicles" exact>
                  <Layout>
                    <Vehicles />
                  </Layout>
                </PrivateRoute>
                <PrivateRoute path="/account" exact>
                  <Layout>
                    <Account />
                  </Layout>
                </PrivateRoute>
              </Switch>
            </CountContextProvider>
          </UserContextProvider>
        </AuthContextProvider>
      </div>
    </BrowserRouter>
  );
}

export default App;
