import React, { useContext } from "react";
import styles from "../home/Home.module.css";

import { UserContext } from "../../context/UserContext";
import { VehicleContext } from "../../context/VehicleContext";
import moment from "moment";

const Home = (props) => {
  const { user } = useContext(UserContext);
  const { vehicleData, vehicleList } = useContext(VehicleContext);

  const latestReport = (vehicleData) => {
    let latestVehicle = { timestamp: 0 };
    let text = "no data available...";
    Object.keys(vehicleData).map((key) => {
      if (vehicleData[key].timestamp > latestVehicle.timestamp) {
        latestVehicle = vehicleData[key];
      }
    });
    if (latestVehicle.vehicleUuid) {
      const lastReportText = moment(latestVehicle.timestamp * 1000).fromNow();
      const vehicle = vehicleList.find(
        (x) => x.uuid === latestVehicle.vehicleUuid
      );
      if (vehicle) {
        text = `${vehicle.year} - ${vehicle.model.make} ${vehicle.model.name} (${lastReportText})`;
      }
    }
    return text;
  };

  const makeRange = (vehicleList) => {
    let makeDict = {};
    let text = "No current make info";
    vehicleList.map((key) => {
      makeDict[key.model.make] = makeDict;
    });
    if (!makeDict.length) {
      text = Object.keys(makeDict).join(", ");
    }
    return text;
  };

  const yearRange = (vehicleList) => {
    let text = "No current year info";
    let maxYear = 0;
    let minYear = 9999;
    vehicleList.map((vehicle) => {
      if (vehicle.year > maxYear) maxYear = vehicle.year;
      if (vehicle.year < minYear) minYear = vehicle.year;
    });
    if (!vehicleList.length) return text;
    if (vehicleList.length) return `${minYear} - ${maxYear}`;
  };

  return (
    <div className={styles.mainCard}>
      <h1 className={styles.head}>
        Welcome Home {user.firstName}, This is the viewer for the FSSE Vehicle
        Simulation
      </h1>
      <ul>
        <li>
          <span className={styles.label}>Number of Vehicles: </span>
          <strong>{vehicleList.length}</strong>
        </li>
        <li>
          <span className={styles.label}>Latest Report: </span>
          <strong>{latestReport(vehicleData)}</strong>
        </li>
        <li>
          <span className={styles.label}>Latest Report: </span>
          <strong>{makeRange(vehicleList)}</strong>
        </li>
        <li>
          <span className={styles.label}>Year Range: </span>
          <strong>{yearRange(vehicleList)}</strong>
        </li>
      </ul>
    </div>
  );
};

export default Home;
