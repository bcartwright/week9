import React, { useState } from "react";
import * as api from "../../utils/api";
import { Redirect, useHistory, Link } from "react-router-dom";
import styles from "../createAccount/CreateAccount.module.css";

const CreateNewUser = () => {
  const history = useHistory();

  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();

    api
      .post({
        route: `/newUser/${email}`,
        body: { firstname, lastname, email, password },
      })
      .then(console.log("User Added"), history.push("/login"))
      .catch((err) => {
        console.log("There was an error with your new user submission");
        console.log(err);
        setFirstname("");
        setLastname("");
        setEmail("");
        setPassword("");
      });
  };

  return (
    <div className={styles.userCard}>
      <div className={styles.head}>
        Fill out the form to create your new user account
      </div>
      <div className={styles.formCard}>
        <form className={styles.form} onSubmit={handleSubmit}>
          <div className={styles.formField}>
            <label htmlFor="firstname">First Name:</label>
            <input
              className={styles.input}
              type="text"
              name="firstname"
              placeholder="John"
              onChange={(e) => {
                setFirstname(e.target.value);
              }}
              value={firstname}
              required
            ></input>
          </div>
          <div className={styles.formField}>
            <label htmlFor="lastname">Last Name:</label>
            <input
              className={styles.input}
              type="text"
              name="lastname"
              placeholder="Doe"
              onChange={(e) => {
                setLastname(e.target.value);
              }}
              value={lastname}
              required
            ></input>
          </div>
          <div className={styles.formField}>
            <label htmlFor="email">Email:</label>
            <input
              className={styles.input}
              type="text"
              name="email"
              placeholder="example@email.com"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              value={email}
              required
            ></input>
          </div>
          <div className={styles.formField}>
            <label htmlFor="password">Password:</label>
            <input
              className={styles.input}
              type="password"
              name="password"
              placeholder="Create yo password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
              required
            ></input>
          </div>
          <button className={styles.button} type="submit">
            Create User
          </button>
          <div style={{ paddingTop: 8 }}>
            <Link to="/login">Go Back</Link>
          </div>
        </form>
      </div>
    </div>
  );
};

export default CreateNewUser;
