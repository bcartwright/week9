import React, { useContext, useState } from "react";
import * as api from "../../utils/api";
import styles from "./vehicles.module.css";
// import moment from "moment";
// import mqtt from "mqtt";

import VehicleCard from "../../lib/vehicle-card/vehicleCard";
import { useMqtt } from "../../custom-hooks/mqtt";

import { AuthContext } from "../../context/AuthContext";
import { UserContext } from "../../context/UserContext";
import { CountContext } from "../../context/CountContext";
import { VehicleContext } from "../../context/VehicleContext";

const Vehicles = () => {
  const [vehicleTopSpeed, setVehicleTopSpeed] = React.useState({});

  const [vin, setVin] = useState("");
  const [name, setName] = useState("");
  const [modelUuid, setModelUuid] = useState("");
  const [year, setYear] = useState("");
  const [color, setColor] = useState("");
  const [imageUrl, setImageUrl] = useState("");

  const {
    vehicleList,
    // setVehicleList,
    vehicleData,
    setVehicleData,
  } = useContext(VehicleContext);
  const { token } = useContext(AuthContext);
  const { user } = useContext(UserContext);
  const { state } = useContext(CountContext);

  useMqtt(
    {
      url: api.wsBaseUrl,
      opts: {
        clientId: "client-" + new Date().getTime().toString(),
        username: user.uuid,
        password: token,
      },
      onmessage: async (topic, payload) => {
        let data = JSON.parse(payload);
        console.log(data);
        setVehicleData((prevState) => {
          return { ...prevState, [data.vehicleUuid]: data };
        });
      },
      onconnect: (client) => {
        console.log("Connected to MQTT");
        vehicleList.map((v) => {
          client.subscribe(`vehicles/${v.uuid}/data`);
        });
      },
    },
    [vehicleList]
  );

  React.useEffect(() => {
    async function onComponentMount() {
      if (state.count == 0) {
        return setVehicleTopSpeed({});
      }

      let jsonData = await api.get({
        route: "/vehicles",
        headers: { Authorization: `Bearer ${token}` },
      });
      let topSpeedValue = {};

      for (let x = 0; x < jsonData.length; x++) {
        let topSpeed = await api.get({
          route: `/vehicles/${jsonData[x].uuid}/top-speed`,
          headers: { Authorization: `Bearer ${token}` },
        });
        topSpeedValue[jsonData[x].uuid] = topSpeed;
      }
      setVehicleTopSpeed(topSpeedValue);
    }
    onComponentMount();
  }, [state.count]);

  const vehicleCardList =
    vehicleList && vehicleList.length ? (
      vehicleList.map((vehicle) => {
        return (
          <VehicleCard
            key={vehicle.uuid}
            vehicle={vehicle}
            vehicleData={vehicleData}
            vehicleTopSpeed={vehicleTopSpeed}
          />
        );
      })
    ) : (
      <p>No vehicles found yet... </p>
    );

  const handleSubmit = (e) => {
    e.preventDefault();

    api
      .post({
        route: `/provision/${vin}`,
        headers: { Authorization: `Bearer ${token}` },
        body: { vin, name, modelUuid, year, color, imageUrl },
      })
      .then(console.log("Vehicle Added"))
      .catch((err) => {
        console.log("There was an error with your vehicle submission");
        console.log(err);
      });
  };

  return (
    <div>
      <h2>Vehicles</h2>
      <div className={styles.vehicleCardContainer}>{vehicleCardList}</div>
      <div>
        <hr
          class="solid"
          style={{ height: 1, backgroundColor: "#14586e", margin: 60 }}
        ></hr>
      </div>
      <div className={styles.userCard}>
        <div className={styles.head}>Create A New Vehicle</div>
        <div className={styles.formCard}>
          <form className={styles.form} onSubmit={handleSubmit}>
            <div className={styles.formField}>
              <label htmlFor="vin">Vin:</label>
              <input
                className={styles.input}
                type="text"
                name="vin"
                placeholder="123456789ABCDEFGH"
                onChange={(e) => {
                  setVin(e.target.value);
                }}
                value={vin}
                required
              ></input>
            </div>
            <div className={styles.formField}>
              <label htmlFor="name">Name:</label>
              <input
                className={styles.input}
                type="text"
                name="name"
                placeholder="Columbus"
                onChange={(e) => {
                  setName(e.target.value);
                }}
                value={name}
                required
              ></input>
            </div>
            <div className={styles.formField}>
              <label htmlFor="modelUuid">Model Uuid:</label>
              <input
                className={styles.input}
                type="text"
                name="modelUuid"
                placeholder="b8a71953-d20d-41da-9b51-84e394c387bc"
                onChange={(e) => {
                  setModelUuid(e.target.value);
                }}
                value={modelUuid}
                required
              ></input>
            </div>
            <div className={styles.formField}>
              <label htmlFor="year">Year:</label>
              <input
                className={styles.input}
                type="text"
                name="year"
                placeholder="2000"
                onChange={(e) => {
                  setYear(e.target.value);
                }}
                value={year}
                required
              ></input>
            </div>
            <div className={styles.formField}>
              <label htmlFor="color">Color:</label>
              <input
                className={styles.input}
                type="text"
                name="color"
                placeholder="blue"
                onChange={(e) => {
                  setColor(e.target.value);
                }}
                value={color}
                required
              ></input>
            </div>
            <div className={styles.formField}>
              <label htmlFor="imageUrl">Image Url</label>
              <input
                className={styles.input}
                type="text"
                name="imageUrl"
                placeholder="https://www...."
                onChange={(e) => {
                  setImageUrl(e.target.value);
                }}
                value={imageUrl}
                required
              ></input>
            </div>
            <button className={styles.button} type="submit">
              Create Vehicle
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Vehicles;
