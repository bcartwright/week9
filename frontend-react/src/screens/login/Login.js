import React, { useContext, useState } from "react";
import styles from "../login/Login.module.css";
import * as api from "../../utils/api";

import { AuthContext } from "../../context/AuthContext";
import { UserContext } from "../../context/UserContext";

import { Redirect, useHistory, Link } from "react-router-dom";

const Login = () => {
  const { setUser } = useContext(UserContext);
  const { setIsAuthenticated, setToken } = useContext(AuthContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();

    api
      .post({ route: "/login", body: { email, password } })
      .then((res) => {
        setUser(res.user);
        setToken(res.token);
        setIsAuthenticated(true);
        setEmail("");
        setPassword("");

        let redirect = new URLSearchParams(window.location.search).get("to");
        redirect = redirect ? redirect : "/home";

        history.push(redirect);
      })
      .catch((err) => {
        console.log("There was a login error:");
        console.log(err);
        setEmail("");
        setPassword("");
      });
  };
  return (
    <div className={styles.loginCard}>
      <div className={styles.head}>Welcome to the Login</div>
      <div className={styles.formCard}>
        <form className={styles.form} onSubmit={handleSubmit}>
          <div className={styles.formField}>
            <label htmlFor="email">Email:</label>
            <input
              className={styles.input}
              type="text"
              name="email"
              placeholder="user@email.com"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              value={email}
              required
            ></input>
          </div>
          <div className={styles.formField}>
            <label htmlFor="password">Password:</label>
            <input
              className={styles.input}
              type="password"
              name="password"
              placeholder="Enter yo password"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
              required
            ></input>
          </div>
          <button className={styles.button} type="submit">
            Sign in
          </button>
        </form>
        <div>
          Dont have an Account? Create one <Link to="/newUser">here</Link>
        </div>
      </div>
    </div>
  );
};
export default Login;
