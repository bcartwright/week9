import React, { useContext, useState } from "react";
import styles from "../account/Account.module.css";
import pippin from "./pip.png";
import * as api from "../../utils/api";
import moment from "moment";

import { UserContext } from "../../context/UserContext";
import { AuthContext } from "../../context/AuthContext";

import { useHistory } from "react-router-dom";

const Account = (props) => {
  const { user, setUser } = useContext(UserContext);
  const { token } = useContext(AuthContext);
  const { setIsAuthenticated, setToken } = useContext(AuthContext);
  const history = useHistory();
  const [inputData, setInputData] = useState("");

  // let imgString = [];
  // var img = new Image();
  // img.src = imgString;

  const handleLogout = (props) => {
    setUser({});
    setToken("");
    setIsAuthenticated(false);
    history.push("/login");
  };
  console.log(user);

  // const uploadImg = (props) => {
  //   let imgData = inputData;
  //   let imgBuff = new Buffer.from(imgData, "utf-8");
  //   let base64Data = imgBuff.toString("base64");

  //   api
  //     .patch({
  //       route: `/users/${user.uuid}/imgData`,
  //       body: { imgData },
  //     })
  //     .then(console.log("Img added successfully"))
  //     .catch((err) => {
  //       console.log("error uploading img");
  //       console.log(err);
  //     });
  // };

  // const getImg = async (props) => {
  //   imgString = await api.get({
  //     route: `/users/${user.uuid}/imgString`,
  //     headers: { Authorization: `Bearer ${token}` },
  //   });

  //   console.log(imgString);
  // };

  return (
    <div className={styles.acctCard}>
      <h1 className={styles.head}>
        Hello there {user.firstName}, here's your account info:
      </h1>
      <div className={styles.infoCard}>
        <img className={styles.acctImg} src={pippin}></img>
        <div>
          <span className={styles.label}>First Name:</span>{" "}
          <strong>{user.firstName}</strong>
        </div>
        <div>
          <span className={styles.label}>Last Name:</span>{" "}
          <strong>{user.lastName}</strong>
        </div>
        <div>
          <span className={styles.label}>Your Email:</span>{" "}
          <strong>{user.email}</strong>
        </div>
        <div>
          <span className={styles.label}>Account Created:</span>{" "}
          <strong>{moment(user.created).fromNow()}</strong>
        </div>
        <div>
          <span className={styles.label}>Last Update:</span>{" "}
          <strong>{moment(user.updated).fromNow()}</strong>
        </div>
        <button className={styles.logOutButton} onClick={handleLogout}>
          Log Out
        </button>
      </div>
      {/* <form></form>
      <label htmlFor="profileImg">Paste img url here:</label>
      <input
        type="text"
        name="profileImg"
        placeholder="url here"
        onChange={(e) => {
          setInputData(e.target.value);
        }}
        value={inputData}
      ></input>
      <button className={styles.logOutButton} onClick={uploadImg}>
        Upload
      </button>
      <button className={styles.logOutButton} onClick={getImg}>
        See Image
      </button> */}
    </div>
  );
};

export default Account;
